
public class GetPasswordString {

        private static final String passwordPassword = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";


        public String passwordType(int numbertype, int lowercasetype, int uppercasetype, int length) {
            int range = 0;//随机密码池里密码的选取
            if (numbertype > 0) {
                if (numbertype > 0 && lowercasetype > 0 && uppercasetype > 0)//选中三种类型密码
                {
                    return getPasswordString(passwordPassword.substring(0, 62), length, 61);
                }
                if (numbertype > 0 && lowercasetype > 0)//选中数字和小写字母的密码
                {
                    return getPasswordString(passwordPassword.substring(0, 36), length, 35);
                }
                if (numbertype > 0 && uppercasetype > 0)//选中数字和大写字母的密码
                {
                    return getPasswordString(passwordPassword.substring(0, 10) + passwordPassword.substring(36, 62), length, 35);
                }
                //只选中数字的密码
                return getPasswordString(passwordPassword.substring(0, 10), length, 9);
            }
          
            
        }
    }
